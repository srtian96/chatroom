import React, { Component } from 'react'
import Logo from '../../component/logo/logo'
import { List, InputItem, WingBlank, WhiteSpace, Button, NavBar} from 'antd-mobile'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { login } from '../../redux/user.redux'

@connect(
    state => state.user,
    { login }
)

class Login extends Component{
    constructor(props) {
        super(props)
        this.state = {
            user:'',
            pwd:'',
        }
        this.register = this.register.bind(this)
        this.handleLogin = this.handleLogin.bind(this)
    }
    register() {
        this.props.history.push('./register')
    }
    handleChange(key,value) {
        this.setState({
            [key]:value
        })
    }
    handleLogin() {
        this.props.login(this.state)
    }
    render() {
        return(
            <div>
               {(this.props.redirectTo&&this.props.redirectTo!=='/login')? <Redirect to={this.props.redirectTo} />:null}
               <NavBar model='dark'>登录页面</NavBar>
                <Logo></Logo>
                <WingBlank>
                {this.props.msg?<p className='error-message'>{this.props.msg}</p>:null}
                <List>
                    <InputItem onChange={v=>this.handleChange('user', v)}>账号</InputItem>
                    <InputItem type='password' onChange={v=>this.handleChange('pwd', v)}>密码</InputItem>
                </List>
                <Button type="primary" onClick={this.handleLogin}>登陆</Button>
                <WhiteSpace />
                <Button onClick={this.register} type="primary">注册</Button>
            </WingBlank>
            </div>
        )
    }
}
export default Login