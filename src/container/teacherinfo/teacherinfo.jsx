import React, { Component } from 'react'
import { NavBar, InputItem, TextareaItem, Button } from "antd-mobile"
import { AvatarSelector } from '../../component/avatar-selector/avatar-selector'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom' 
import { update } from "../../redux/user.redux"
@connect(
    state=>state.user,
    {update}
)
class TeacherInfo extends Component{
    constructor(props) {
        super(props)
        this.state={
            name:'',
            money:'',
            subject:'',
            introduce:'',
        }
    }
    onChange(key,value) {
        this.setState({
            [key]:value
        })
    }
    render() {
        const path = this.props.location.pathname
        const redirect = this.props.redirectTo
        return(
            <div>
                {redirect&&redirect!==path?<Redirect to={this.props.redirectTo}></Redirect>:null}
                <NavBar mode="dark">老师完善信息页面</NavBar>
                <AvatarSelector selectAvatar={imgname=>{
                    this.setState({
                        avatar:imgname
                    })
                }}>
                </AvatarSelector>
                <InputItem onChange={(v)=>this.onChange('name', v)}>老师姓名：</InputItem>       
                <InputItem onChange={(v)=>this.onChange('subject', v)}>所教科目：</InputItem>
                <InputItem onChange={(v)=>this.onChange('money', v)}>补习价格：</InputItem>
                <TextareaItem 
                onChange={v=>this.onChange('introduce', v)}
                rows={3}
                autoHeight
                title='自我介绍'
                />
                <Button type="primary" onClick={()=>{this.props.update(this.state)}}>提交</Button>   
            </div>
        )
    }
}
export default TeacherInfo