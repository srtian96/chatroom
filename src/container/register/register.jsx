import React, { Component } from 'react'
import Logo from '../../component/logo/logo'
import { List, InputItem, WingBlank, WhiteSpace, Button, Radio, NavBar, Icon} from 'antd-mobile'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { registry } from '../../redux/user.redux'

@connect(
	state => state.user,
	{ registry }
)
class Register extends Component{
    constructor(props) {
        super(props)
        this.state = {
            user:'',
            pwd:'',
            repeatpwd:'',
            type:'teacher'
        }
        this.login = this.login.bind(this)
        this.handleRegister = this.handleRegister.bind(this)
    }
    login() {
        this.props.history.push('./login')
    }
    handleChange(key, value) {
        this.setState({
            [key]:value
        })
    }
    handleRegister() {
        this.props.registry(this.state)
    }
    render() {
        const RadioItem = Radio.RadioItem
        return(
            <div>
                {this.props.redirectTo ? <Redirect to={this.props.redirectTo} /> : null}
                <NavBar 
                model='dark' icon={<Icon type="left" />} 
                onLeftClick={()=>{this.props.history.goBack()}}
                >
                注册页面
                </NavBar>
                <Logo></Logo>
                <WingBlank>
                    <List>
                        {this.props.msg?<p className='error-message'>{this.props.msg}</p>:null}
                        <InputItem onChange={v=>this.handleChange('user', v)}>账号</InputItem>
                        <InputItem type='password' onChange={v=>this.handleChange('pwd', v)}>密码</InputItem>
                        <InputItem type='password' onChange={v=>this.handleChange('repeatpwd', v)}>确定密码</InputItem>
                        <WhiteSpace />
                        <RadioItem 
                        checked={this.state.type==='student'} 
                        onChange={()=>this.handleChange('type', 'student')}>
                        学生
                        </RadioItem>
                        <RadioItem 
                        checked={this.state.type==='teacher'}
                        onChange={()=>this.handleChange('type', 'teacher')}>
                        老师
                        </RadioItem>
                    </List>
                    <WhiteSpace />
                    <Button onClick={this.handleRegister} type="primary">注册</Button>
                    <WhiteSpace />
                    <Button onClick={this.login} type="primary">返回</Button>
                </WingBlank>
            </div>
        )
    }
}

export default Register