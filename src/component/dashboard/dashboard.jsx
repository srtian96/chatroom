import React, { Component } from 'react'
import { connect } from 'react-redux'
import {NavBar} from 'antd-mobile'
import {Switch, Route} from 'react-router-dom'
import  NavLinkBar  from '../navlink/navlink'
import  Student  from '../../component/student/student'
import  Teacher  from '../../component/teacher/teacher'
import  Msg  from '../msg/msg'
import  Personal  from '../../component/personal/personal'
import {getMsgList, receiveMsg} from '../../redux/chat.redux'

@connect(
    state => state,
    { getMsgList, receiveMsg} 
)

class Dashboard extends Component{
     componentDidMount() {
         if (!this.props.chat.chatmsg.length) {
            this.props.getMsgList()
            this.props.receiveMsg()
         }
     }
    render() {
        const user = this.props.user
        const {pathname} = this.props.location
        const navList= [
            {
                path:'/teacher',
                text:'老师',
                icon:'teacher',
                title:'老师名单',
                component:Student,
                hide:user.type==='teacher',
            },
            {
                path:'/student',
                text:'学生',
                icon:'student',
                title:'学生名单',
                component:Teacher,
                hide:user.type==='student',
            },
            {
                path:'/msg',
                text:'聊天',
                icon:'msg',
                title:'聊天中心',
                component:Msg,
            },
            {
                path:'/me',
                text:'我',
                icon:'user',
                title:'个人中心',
                component:Personal,
            }
        ]
        return(
			<div>
				<NavBar className="fixed-header" mode="dark">{navList.find(v => v.path===pathname).title}</NavBar>
				<div style={{marginTop:50}}>
						<Switch>
							{navList.map(v => (
								<Route key={v.path} path={v.path} component={v.component}></Route>
							))}
						</Switch>
				</div>
				<NavLinkBar data={navList}></NavLinkBar>			
			</div>
		)
    }
}

export default Dashboard