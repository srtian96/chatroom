import React,{ Component } from 'react'
import { connect } from 'react-redux'

import {Card, WingBlank} from 'antd-mobile'
import { getUserList } from '../../redux/userlist.redux'

@connect(
    state => state.list,
    { getUserList }
)
class Student extends Component {
       constructor(props) {
        super(props)
        this.state={
            data:[]
        }
    }
    componentDidMount() {
      this.props.getUserList('teacher')
    }
    handleClick(v) {
        this.props.history.push(`/chat/${v._id}`)
    }
    render() {
        const Body = Card.Body 
        const Header = Card.Header
        console.log('user.list', this.props.userlist)
        return(
            <WingBlank>
                {this.props.userlist.map(v => (
                    v.avatar?<Card key={v._id} onClick={()=>this.handleClick(v)}>
                        <Header
                        title={v.user} 
                        thumb={require(`../images/${v.avatar}.png`)}
                        extra={<p>科目：{v.subject}</p>}
                        >
                        </Header>
                        <Body>
                            <span>补习价格：{v.money}</span>
                            <p>个人介绍：</p>
                            {v.introduce.split('\n').map(e => (
                                    <div key={e}>{e}</div>
                                ))}
                        </Body>
                    </Card>:null
                ))}
            </WingBlank>
        )
    }
}

export default Student