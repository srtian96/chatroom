import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Result, List, WhiteSpace, Modal, Button} from 'antd-mobile'
import browserCookie from 'browser-cookies'
import  { logoutSubmit } from '../../redux/user.redux'
import { Redirect } from 'react-router-dom'
@connect(
	state => state.user,
	{ logoutSubmit }
)
export default class Personal extends Component{
	constructor(props){
		super(props)
		this.logout = this.logout.bind(this)
	}
	logout() {
		const alert = Modal.alert
		alert('退出', '确定退出登录？', [
		      { text: '取消', onPress: () => console.log('cancel') },
		      { text: '确认', onPress: () => {
		      	browserCookie.erase('userid')
		      	this.props.logoutSubmit()
		      }}
		    ])
	}
	render() {
		const props = this.props
		const Item = List.Item
		return props.user?(
			<div>
				<Result
					img={<img src={require(`../images/${props.avatar}.png`)} style={{width:50}} alt="" />}
					title={props.user}
					message={props.subject}
				/>
				<List renderHeader={()=>'简介'}>
					<Item multipleLine wrap>
						{
						props.type==='student'?
						<div><span>弱点科目: </span>{props.subject}</div>
						:<div><span>所教科目：</span>{props.subject}</div>
						}
						<p>个人介绍:</p>
                        <span>{props.introduce}</span>
					</Item>
				</List>
				<WhiteSpace></WhiteSpace>
				<Button type="primary" onClick={this.logout}>退出登录</Button>
			</div>
		):<Redirect to={props.redirectTo} />
	}
}