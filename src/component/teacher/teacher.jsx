import React,{ Component } from 'react'
import { connect } from 'react-redux'
import {Card, WingBlank} from 'antd-mobile'

import { getUserList } from '../../redux/userlist.redux'

@connect(
    state=>state.list,
    { getUserList }
)
class Teacher extends Component {
       constructor(props) {
        super(props)
        this.state={
            data:[]
        }
    }
    componentDidMount() {
		console.log(11111111)
      this.props.getUserList('student')
    }
    handleClick(v) {
        this.props.history.push(`/chat/${v._id}`)
    }
    render() {
        const Body = Card.Body
        const Header = Card.Header
        return(
            <WingBlank>
                {this.props.userlist.map(v=>(
                    v.avatar?<Card key={v._id} onClick={()=>this.handleClick(v)}>
                        <Header
                        title={v.user}
                        thumb={require(`../images/${v.avatar}.png`)}
                        extra={<p>科目：{v.subject}</p>}
                        >
                        </Header>
                        <Body>
                            <div>
                            <span>课时价格:</span>{v.money}
                            <p>个人介绍：</p>
                            {v.introduce.split('\n').map(e=>(
                                    <div key={e}>{e}</div>
                                ))}
                            </div>
                        </Body>
                    </Card>:null
                ))}
            </WingBlank>
        )
    }
}

export default Teacher