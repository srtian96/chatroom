import React, { Component } from 'react'
import logoPng from './logo.png'
import './logo.css'
class Logo extends Component{
    render() {
        return(
            <div className="logo-center">
                <img src={logoPng} alt="logo"/>
            </div>
        )
    }
}

export default Logo