import React, { Component } from 'react'
import {Grid , List} from 'antd-mobile'

export class AvatarSelector extends Component{
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
        const avatarList= '奥特曼,哆啦A梦,皮卡丘,索隆,路飞,龙猫,大白,微笑'.split(',').map(v => ({
                              icon:require(`../images/${v}.png`),
                              text:v
                          }))
        const setAvatar = this.state.icon?
        (<div>
            <span>已选择头像</span>
            <img style={{width:30}} src={this.state.icon} alt="头像"/>
        </div>):'请选择头像'
        return(
            <div>
                <List renderHeader={()=>setAvatar}>
                    <Grid 
                    data={avatarList}
                    onClick={
                        ele => {
                            this.setState(ele)
                            this.props.selectAvatar(ele.text)
                        }
                    }
                    />
                </List>
            </div>
        )
    }
}