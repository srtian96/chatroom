这本是一个个人项目，想做成一个聊天室的。但由于某些原因，改为大学生创新创业的一个项目，因此最终项目与本项目有所出入。

使用的技术栈：
- react
- redux
- react-route
- express
- antd-mobile
- socket.io
- axios
